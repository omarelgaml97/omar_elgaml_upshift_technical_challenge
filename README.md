# omar_Elgaml_upshift_technical_challenge

This is a brief description of the project.

## The application features :

1. Login screen
2. Home screen showing the posts
3. Clicking on a post will open its details to edit.
4. Clicking on the delete icon will delete the post.
5. Clicking on the add icon will open a screen to add a post.
6. From the Drawer, The user can see his/her profile.
7. Logout button delets the fake user.

## Used third-part Libraries

1. https://www.npmjs.com/package/react-native-flash-message --> to show notification messages to the user
2. https://www.npmjs.com/package/react-native-keyboard-aware-scroll-view --> To handle keyboard appearance and automatically scrolls to focused TextInput

## Prerequisites

- Node.js(version 12 or higher)
- Expo CLI (version 4 or higher)

## Installation

1. Clone the repository
2. install the project dependencies : yarn install
3. run the project in development mod :expo start

## To run the app on a physical device, follow these steps:

1. Install the Expo app on your device (available on Android and iOS).
2. Make sure your device and your computer are connected to the same Wi-Fi network.
3. Scan the QR code that appears in your terminal or in the web page opened by Expo with your device's camera app. This will open the app in the Expo app on your device.

## To run the app on an emulator, follow these steps:

1. Make sure you have an emulator installed on your machine and it is running.
2. Click the "Run on Android device/emulator" or "Run on iOS simulator" button in the web page opened by Expo.
3. This will launch the app on the emulator.
