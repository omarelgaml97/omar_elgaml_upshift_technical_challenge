import * as React from 'react';
import HomeStack from '@app/navigation/HomeStack';
import Profile from '@app/views/screens/profile/Profile';
import { useAppDispatch } from '@app/state/Hooks';

import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';

import { MainStackParamList } from './ParamLists';
import { TouchableOpacity, Text } from 'react-native';
import { logout } from '../state/Thunks';
const Drawer = createDrawerNavigator<MainStackParamList>();

function CustomDrawerContent(props: any) {
  const dispatch = useAppDispatch();
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <TouchableOpacity onPress={() => dispatch(logout())}>
        <Text style={{ margin: 16, fontWeight: 'bold', fontSize: 16 }}>
          Logout
        </Text>
      </TouchableOpacity>
    </DrawerContentScrollView>
  );
}

function MainNavigator() {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen
        name="HomeStack"
        component={HomeStack}
        options={{ title: 'Home' }}
      />
      <Drawer.Screen name="Profile" component={Profile} />
    </Drawer.Navigator>
  );
}

export default MainNavigator;
