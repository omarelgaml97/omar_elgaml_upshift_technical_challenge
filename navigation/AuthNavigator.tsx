import * as React from 'react';
import Login from '@app/views/screens/login/Login';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { AuthStackParamList } from '@app/navigation/ParamLists';
const Stack = createNativeStackNavigator<AuthStackParamList>();

function AuthNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ title: 'Login' }}
      />
    </Stack.Navigator>
  );
}

export default AuthNavigator;
