import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MainNavigator from '@app/navigation/MainNavigator';
import { UserSelector } from '@app/state/Selectors';
import { useAppSelector } from '@app/state/Hooks';
import AuthNavigator from '@app/navigation/AuthNavigator';
function Navigation() {
  const user = useAppSelector((state) => UserSelector(state));

  return (
    <NavigationContainer>
      {Object.keys(user).length === 0 ? <AuthNavigator /> : <MainNavigator />}
    </NavigationContainer>
  );
}

export default Navigation;
