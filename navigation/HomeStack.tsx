import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HomeStackParamList } from '@app/navigation/ParamLists';
import HomeScreen from '@app/views/screens/home/Home';
import PostDetails from '@app/views/screens/postDetails/PostDetails';

const Stack = createNativeStackNavigator<HomeStackParamList>();
function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PostDetails"
        component={PostDetails}
        options={{ title: 'Details' }}
      />
    </Stack.Navigator>
  );
}

export default HomeStack;
