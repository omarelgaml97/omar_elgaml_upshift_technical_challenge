import { Post } from '@app/utils/Types';

export type MainStackParamList = {
  HomeStack: undefined;
  Profile: undefined;
};
export type HomeStackParamList = {
  Home: undefined;
  CreatePost: undefined;
  PostDetails: { Post: Post | { title: string; body: string } };
};
export type AuthStackParamList = {
  Login: undefined;
};
