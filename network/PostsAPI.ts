import axios from '@app/network/axios';

import { Post, newPost } from '@app/utils/Types';

const PostsAPI = {
  getAllPosts: async (): Promise<Post[]> => {
    const response = await axios.get<Post[]>('/posts');
    return response.data;
  },

  updatePost: async (body: { post: Post | {}; id: number }): Promise<Post> => {
    const response = await axios.put<Post>(`/posts/${body.id}`, body.post);
    return response.data;
  },

  deletePost: async (id: number): Promise<number> => {
    await axios.delete<Post>(`/posts/${id}`);

    return id;
  },

  addPost: async (body: newPost): Promise<Post> => {
    const response = await axios.post<Post>(`/posts/`, body);

    return response.data;
  },
};

export default PostsAPI;
