import { Props } from './TextBox.props';
import { StyledTextInput } from '@app/views/components/textBox/TextBox.style';
import { View } from 'react-native';
const TextBox: React.FC<Props> = ({
  placeholder,
  value,
  multiline,
  onChange,
  width,
  height,
}) => {
  const handleTextChange = (text: string) => {
    if (onChange) {
      onChange(text);
    }
  };

  return (
    <View>
      <StyledTextInput
        placeholder={placeholder}
        value={value}
        onChangeText={handleTextChange}
        multiline={multiline}
        numberOfLines={multiline ? 4 : 1}
        width={width}
        height={height}
      />
    </View>
  );
};

export default TextBox;
