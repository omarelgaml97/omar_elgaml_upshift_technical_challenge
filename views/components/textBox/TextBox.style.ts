import styled from 'styled-components/native';
import { StyledTextInputProps } from '@app/views/components/textBox/TextBox.props';

export const TextBoxContainer = styled.View`
  margin-bottom: 16px;
`;

export const StyledTextInput = styled.TextInput<StyledTextInputProps>`
  background-color: #fff;
  border-width: 1px;
  border-color: #ccc;
  border-radius: 8px;
  padding: 16px;
  font-size: 16px;
  ${({ width }: { width?: string }) => width && `width: ${width};`}
  ${({ height }: { height?: string }) => height && `height: ${height};`}
  margin-bottom:24px;
`;
