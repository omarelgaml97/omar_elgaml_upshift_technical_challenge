export interface Props {
  placeholder?: string;
  value?: string;
  multiline?: boolean;
  onChange?: (text: string) => void;
  width?: string;
  height?: string;
}

export interface StyledTextInputProps {
  width?: string;
  height?: string;
}
