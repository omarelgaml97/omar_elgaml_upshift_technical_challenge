import { ActivityIndicator } from 'react-native';
import {
  Container,
  LoadingText,
} from '@app/views/components/loading/Loading.styles';

const LoadingIndicator = () => {
  return (
    <Container>
      <ActivityIndicator size="large" color="#0000ff" />
      <LoadingText>Loading...</LoadingText>
    </Container>
  );
};

export default LoadingIndicator;
