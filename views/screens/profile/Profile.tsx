import React from 'react';
import { useAppSelector } from '@app/state/Hooks';
import { UserSelector } from '@app/state/Selectors';
import {
  Row,
  Column,
  Label,
  Container,
  Value,
} from '@app/views/screens/profile/Profile.styles';

import { MainStackParamList } from '@app/navigation/ParamLists';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
type ProfileScreenProps = NativeStackScreenProps<MainStackParamList, 'Profile'>;

const ProfileScreen: React.FC<ProfileScreenProps> = () => {
  const user = useAppSelector((state) => UserSelector(state));
  return (
    <>
      {Object.keys(user).length > 0 && (
        <Container>
          <></>
          <Row>
            <Column>
              <Label>Name:</Label>
            </Column>
            <Column>
              <Value>{user.name}</Value>
            </Column>
          </Row>
          <Row>
            <Column>
              <Label>Email:</Label>
            </Column>
            <Column>
              <Value>{user.email}</Value>
            </Column>
          </Row>
          <Row>
            <Column>
              <Label>ID:</Label>
            </Column>
            <Column>
              <Value>{user.id}</Value>
            </Column>
          </Row>
          <Row>
            <Column>
              <Label>Date Joined:</Label>
            </Column>
            <Column>
              <Value>{user.dateJoined}</Value>
            </Column>
          </Row>
        </Container>
      )}
    </>
  );
};

export default ProfileScreen;
