import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  padding: 20px;
  flex-direction;coulmn;
  justify-content:center
`;

export const Row = styled.View`
  flex-direction: row;
  margin-bottom: 32px;
`;

export const Column = styled.View`
  flex: 1;
`;

export const Label = styled.Text`
  font-weight: bold;
`;

export const Value = styled.Text`
  margin-left: 10px;
`;
