import styled from 'styled-components/native';

export const Container = styled.View`
  padding: 8px;
  padding-top: 32px;
  flex-direction: column;
  flex: 1;
`;
export const StyledButton = styled.TouchableOpacity`
  background-color: white;
  padding: 10px;
  border-radius: 5px;
  width: 80%;
`;
export const Title = styled.Text`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 8px;
`;

export const ButtonContainer = styled.View`
  width: 100%;
  flex-direction: row;
  text-align: center;
  justify-content: center;
`;
