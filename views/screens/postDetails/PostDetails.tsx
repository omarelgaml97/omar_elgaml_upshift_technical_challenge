import React from 'react';
import MyTextBox from '../../components/textBox/TextBox';
import {
  Container,
  StyledButton,
  Title,
  ButtonContainer,
} from './PostDetails.styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Text, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { HomeStackParamList } from '@app/navigation/ParamLists';
import { useAppDispatch, useAppSelector } from '@app/state/Hooks';
import { PostsLoadingSelector } from '@app/state/Selectors';
import LoadingIndicator from '@app/views/components/loading/Loading';
import { addPost, updatePost } from '@app/state/Thunks';
import { showMessage } from 'react-native-flash-message';

type PostDetailsScreenProps = NativeStackScreenProps<
  HomeStackParamList,
  'PostDetails'
>;

const PostDetails: React.FC<PostDetailsScreenProps> = (props) => {
  const [post, setPost] = React.useState<any>();

  const [edit, setEdit] = React.useState<boolean>();

  const dispatch = useAppDispatch();

  const loaidngPosts = useAppSelector((state) => PostsLoadingSelector(state));

  React.useEffect(() => {
    setEdit(post?.id || false);
  }, [post]);

  React.useEffect(() => {
    props.navigation.setOptions({ title: edit ? 'Edit Post' : 'New Post' });
  }, [edit]);

  React.useEffect(() => {
    setPost(props.route.params.Post);
  }, []);

  const handleActionClick = async () => {
    if (edit) dispatch(updatePost({ post, id: post?.id }));
    else {
      if (post.title.trim().length < 4 || post.body.trim().length < 30) {
        showMessage({
          message: 'Error',
          description:
            'Please enter a title more than 4 chars and body more than 30 chars',
          type: 'danger',
        });
      } else {
        await dispatch(
          addPost({ title: post?.title, body: post?.body, useId: 1 }),
        );
        props.navigation.goBack();
      }
    }
  };

  const handleTitleChange = (text: string) => {
    let tmp = { ...post };

    tmp.title = text;

    setPost(tmp);
  };

  const handleBodyChange = (text: string) => {
    let tmp = { ...post };

    tmp.body = text;

    setPost(tmp);
  };

  return (
    <KeyboardAwareScrollView>
      {loaidngPosts ? (
        <LoadingIndicator />
      ) : (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <Container>
            <Title>Title: </Title>
            <MyTextBox
              placeholder="Post title"
              value={post?.title}
              onChange={(text: string) => handleTitleChange(text)}
              width="100%"
              multiline
            />
            <Title>Body: </Title>

            <MyTextBox
              placeholder="Post body"
              value={post?.body}
              onChange={(text: string) => handleBodyChange(text)}
              width="100%"
              multiline
            />

            <ButtonContainer>
              <StyledButton onPress={handleActionClick}>
                <Text style={{ color: 'black', textAlign: 'center' }}>
                  {`${edit ? 'Update' : 'Add'}`}
                </Text>
              </StyledButton>
            </ButtonContainer>
          </Container>
        </TouchableWithoutFeedback>
      )}
    </KeyboardAwareScrollView>
  );
};

export default PostDetails;
