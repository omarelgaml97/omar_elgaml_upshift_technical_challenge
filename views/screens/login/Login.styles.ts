import styled from 'styled-components/native';

export const Container = styled.View`
  padding: 8px;
  flex-direction: column;
`;
export const StyledButton = styled.TouchableOpacity`
  background-color: white;
  padding: 10px;
  border-radius: 10px;
  width: 75%;
`;

export const ButtonContainer = styled.View`
  width: 100%;
  flex-direction: row;
  text-align: center;
  justify-content: center;
  margin-top: 32px;
`;
