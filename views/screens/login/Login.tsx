import * as React from 'react';
import { Text, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { AuthStackParamList } from '@app/navigation/ParamLists';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  Container,
  ButtonContainer,
  StyledButton,
} from '@app/views/screens/login/Login.styles';
import MyTextBox from '@app/views/components/textBox/TextBox';
import { useAppDispatch, useAppSelector } from '@app/state/Hooks';
import { UserLoadingSelector } from '@app/state/Selectors';
import LoadingIndicator from '@app/views/components/loading/Loading';
import { login } from '@app/state/Thunks';
import { showMessage } from 'react-native-flash-message';

type LogininScreenProps = NativeStackScreenProps<AuthStackParamList, 'Login'>;
const LoginScreen: React.FC<LogininScreenProps> = (props) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassowrd] = React.useState('');
  const dispatch = useAppDispatch();
  const loading = useAppSelector((state) => UserLoadingSelector(state));

  const handleEmailChange = (text: string) => {
    setEmail(text);
  };
  const handlePassChange = (text: string) => {
    setPassowrd(text);
  };
  const handleActionClick = () => {
    if (email.toLowerCase() == 'admin@admin.com' && password == '123456')
      dispatch(login());
    else
      showMessage({
        message: 'Error',
        description: 'Wrong credentials',
        type: 'danger',
      });
  };
  return (
    <KeyboardAwareScrollView
      contentContainerStyle={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
      }}
    >
      {loading ? (
        <LoadingIndicator />
      ) : (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <Container>
            <MyTextBox
              placeholder="Email"
              value={email}
              onChange={(text: string) => handleEmailChange(text)}
              width="100%"
              multiline={false}
            />

            <MyTextBox
              placeholder="Password"
              multiline={false}
              onChange={(text: string) => handlePassChange(text)}
              width="100%"
            />

            <ButtonContainer>
              <StyledButton onPress={handleActionClick}>
                <Text style={{ color: 'black', textAlign: 'center' }}>
                  Login
                </Text>
              </StyledButton>
            </ButtonContainer>
          </Container>
        </TouchableWithoutFeedback>
      )}
    </KeyboardAwareScrollView>
  );
};
export default LoginScreen;
