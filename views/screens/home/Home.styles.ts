import styled from 'styled-components/native';
import { View } from 'react-native';

export const Container = styled(View)`
  flex: 1;
  background-color: white;
  padding: 12px;
  padding-top: 32px;
`;
export const AddContainer = styled(View)`
  flex-direction: row;
  justify-content: flex-end;
  margin-bottom: 12px;
`;
