import { Post } from '@app/utils/Types';

export interface Props {
  post: Post;
  onEdit: (post: Post) => void;
  onDelete: (id: number) => void;
}
