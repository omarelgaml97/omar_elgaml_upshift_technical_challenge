import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  background-color: #eee;
  border-radius: 16px;
  padding: 16px;
  margin-bottom: 46px;
  flex-direction: column; /* changed to column */
  justify-content: space-between;
  align-items: stretch; /* changed to stretch */
`;
export const TextContainer = styled.View`
  flex: 1;
`;

export const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 8px;
`;

export const Description = styled.Text`
  font-size: 16px;
  margin-bottom: 8px;
`;

export const ButtonContainer = styled.View`
  flex-direction: column;

  align-items: flex-end;
`;

export const EditButton = styled.TouchableOpacity`
  margin-left: 16px;
`;

export const DeleteButton = styled.TouchableOpacity`
  margin-left: auto;
`;
