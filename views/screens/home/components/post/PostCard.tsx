import { Props } from '@app/views/screens/home/components/post/PostCard.props';
import {
  Container,
  Description,
  Title,
  ButtonContainer,
  TextContainer,
  DeleteButton,
} from './PostCard.styles';
import { View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const PostCard: React.FC<Props> = ({ post, onEdit, onDelete }) => {
  return (
    <Container onPress={() => onEdit(post)}>
      <View>
        <Title>{post.title}</Title>
        <Description>{post.body}</Description>
      </View>
      <ButtonContainer>
        <DeleteButton onPress={() => onDelete(post.id)}>
          <Ionicons name="trash-outline" size={24} color="black" />
        </DeleteButton>
      </ButtonContainer>
    </Container>
  );
};

export default PostCard;
