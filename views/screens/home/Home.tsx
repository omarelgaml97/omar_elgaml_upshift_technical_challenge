import * as React from 'react';
import { FlatList } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { HomeStackParamList } from '@app/navigation/ParamLists';
import { useAppDispatch, useAppSelector } from '@app/state/Hooks';
import { deletePost, getPosts } from '@app/state/Thunks';
import { PostsSelector, PostsLoadingSelector } from '@app/state/Selectors';
import { Container, AddContainer } from './Home.styles';
import PostCard from '@app/views/screens/home/components/post/PostCard';
import { Post } from '@app/utils/Types';
import LoadingIndicator from '@app/views/components/loading/Loading';

import { Ionicons } from '@expo/vector-icons';

type HomeScreenProps = NativeStackScreenProps<HomeStackParamList, 'Home'>;

const HomeScreen: React.FC<HomeScreenProps> = (props: HomeScreenProps) => {
  const dispatch = useAppDispatch();
  const posts = useAppSelector((state) => PostsSelector(state));
  const loaidngPosts = useAppSelector((state) => PostsLoadingSelector(state));

  React.useEffect(() => {
    if (!posts.length) dispatch(getPosts());
  }, []);
  const handleDelete = (item: number) => {
    dispatch(deletePost(item));
  };
  const renderItem = ({ item }: { item: Post }) => (
    <PostCard
      onEdit={(post) => props.navigation.push('PostDetails', { Post: post })}
      onDelete={(id) => handleDelete(id)}
      post={item}
    />
  );
  return (
    <Container>
      {loaidngPosts ? (
        <LoadingIndicator />
      ) : (
        <>
          <AddContainer>
            <Ionicons
              name="add-outline"
              size={24}
              color="black"
              onPress={() =>
                props.navigation.push('PostDetails', {
                  Post: { title: '', body: '' },
                })
              }
            />
          </AddContainer>
          <FlatList data={posts} renderItem={renderItem} />
        </>
      )}
    </Container>
  );
};
export default HomeScreen;
