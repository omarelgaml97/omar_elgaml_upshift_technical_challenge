import Navigation from '@app/navigation/Navigation';
import { Provider } from 'react-redux';
import 'react-native-gesture-handler';
import { store } from '@app/state/Store';
import FlashMessage from 'react-native-flash-message';
export default function App() {
  return (
    <Provider store={store}>
      <Navigation />
      <FlashMessage position="top" />
    </Provider>
  );
}
