export interface Post {
  title: string;
  body: string;
  userId: number;
  id: number;
}
export interface newPost {
  useId: number;
  title: string;
  body: string;
}
export interface User {
  name: string;
  id: string;
  email: string;
  dateJoined: string;
}
