import { configureStore } from '@reduxjs/toolkit';
import PostsReducer from '@app/state/PostsSlice';
import UserReducer from '@app/state/UserSlice';

export const store = configureStore({
  reducer: {
    posts: PostsReducer,
    user: UserReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
