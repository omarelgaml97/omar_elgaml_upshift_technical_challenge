import type { RootState } from '@app/state/Store';

export const PostsSelector = (state: RootState) => state.posts.posts;
export const PostsLoadingSelector = (state: RootState) => state.posts.loading;
export const UserSelector = (state: RootState) => state.user.user;
export const UserLoadingSelector = (state: RootState) => state.user.loading;
