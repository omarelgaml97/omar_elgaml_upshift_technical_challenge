import { createAsyncThunk } from '@reduxjs/toolkit';
import { Post, newPost } from '@app/utils/Types';
import PostsAPI from '@app/network/PostsAPI';
export const getPosts = createAsyncThunk<Post[]>('posts/getPosts', async () => {
  const response = await PostsAPI.getAllPosts();
  return response;
});
export const updatePost = createAsyncThunk<
  Post,
  { post: Post | {}; id: number }
>('posts/updatePost', async (body) => {
  const response = await PostsAPI.updatePost(body);
  return response;
});

export const deletePost = createAsyncThunk<number, number>(
  'posts/deletePost',
  async (id) => {
    const response = await PostsAPI.deletePost(id);
    return response;
  },
);

export const addPost = createAsyncThunk<Post, newPost>(
  'posts/addPost',
  async (body) => {
    const response = await PostsAPI.addPost(body);
    return response;
  },
);

export const login = createAsyncThunk<Object>('user/login', async () => {
  const user = {
    name: 'omar elgaml',
    id: '1',
    email: 'example@test.com',
    dateJoined: '4/4/2023',
  };
  return user;
});

export const logout = createAsyncThunk('user/logout', async () => {
  return;
});
