import { Post } from '@app/utils/Types';

export interface PostsState {
  posts: Post[];
  loading: boolean;
}
export interface UserState {
  user: any;
  loading: boolean;
}
