import { createSlice } from '@reduxjs/toolkit';
import { login, logout } from '@app/state/Thunks';
import { UserState } from '@app/state/Types';

const initialState: UserState = {
  user: {},
  loading: false,
};

export const userSlice = createSlice({
  name: 'user',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(login.fulfilled, (state, action) => {
      state.loading = false;
      state.user = action.payload;
    });

    builder.addCase(login.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(logout.fulfilled, (state, action) => {
      state.user = {};
    });
  },
});
export default userSlice.reducer;
