import { createSlice } from '@reduxjs/toolkit';
import { getPosts, updatePost, deletePost, addPost } from '@app/state/Thunks';
import { PostsState } from '@app/state/Types';
import { showMessage } from 'react-native-flash-message';

const initialState: PostsState = {
  posts: [],
  loading: false,
};

export const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getPosts.fulfilled, (state, action) => {
      state.loading = false;
      state.posts = action.payload;
    });
    builder.addCase(getPosts.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(getPosts.rejected, (state, action) => {
      state.loading = false;
      showMessage({
        message: 'Error',
        description: 'An error occured',
        type: 'danger',
      });
    });
    builder.addCase(updatePost.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(updatePost.rejected, (state, action) => {
      state.loading = false;
      showMessage({
        message: 'Error',
        description: 'An error occured',
        type: 'danger',
      });
    });
    builder.addCase(updatePost.fulfilled, (state, action) => {
      state.loading = false;
      let tmp = [...state.posts];
      let newPost = action.payload;
      var foundIndex = tmp.findIndex((x) => x.id == newPost.id);
      tmp[foundIndex] = newPost;
      state.posts = tmp;
      showMessage({
        message: 'Success',
        description: 'Post Updated!',
        type: 'success',
      });
    });
    builder.addCase(deletePost.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(deletePost.rejected, (state, action) => {
      state.loading = false;
      showMessage({
        message: 'Error',
        description: 'An error occured',
        type: 'danger',
      });
    });
    builder.addCase(deletePost.fulfilled, (state, action) => {
      state.loading = false;
      let tmp = [...state.posts];
      let id = action.payload;
      var foundIndex = tmp.findIndex((x) => x.id == id);
      tmp.splice(foundIndex, 1);
      state.posts = tmp;
      showMessage({
        message: 'Success',
        description: 'Post Deleted!',
        type: 'success',
      });
    });

    builder.addCase(addPost.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(addPost.rejected, (state, action) => {
      state.loading = false;
      showMessage({
        message: 'Error',
        description: 'An error occured',
        type: 'danger',
      });
    });
    builder.addCase(addPost.fulfilled, (state, action) => {
      state.loading = false;
      let newPost = action.payload;
      let tmp = [newPost].concat(state.posts);

      state.posts = tmp;
      showMessage({
        message: 'Success',
        description: 'Post Added!',
        type: 'success',
      });
    });
  },
});
export default postsSlice.reducer;
